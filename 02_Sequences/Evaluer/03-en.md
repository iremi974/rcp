What is the value of the `leap` variable at the end of running the following Python program?

```python
per4 = 2024 % 4 == 0
per100 = 2024 % 100 == 0
per1000 = 2024 % 1000 == 0
leap = per4 and not per100 or per1000
```

