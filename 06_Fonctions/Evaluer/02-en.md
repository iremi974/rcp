What is the value of the expression: g(5,3) with the following Python function definition?

```python
def g(a,b):
    r = 1
    for i in range(b):
        r = r * a
    return (r)
```

