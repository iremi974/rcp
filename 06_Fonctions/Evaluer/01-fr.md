Quelle est la valeur à l'évaluation de l'expression : f(-14) avec la définition de fonction Python suivante ?

```python
def f(x):
    if x > 0:
        return (x)
    else:
        return (-x)
```
