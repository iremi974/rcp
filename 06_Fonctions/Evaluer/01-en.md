What is the value of the expression: f(-14) with the following Python function definition?

```python
def f(x):
    if x > 0:
        return (x)
    else:
        return (-x)
```

