The following assignment is considered:

``python
masses = [12, 30, 50, 120, 150, 200]
````

Write an expression to calculate the table of weights corresponding to the masses in the "mass" table. It is recalled that a mass of `10 kg` has a weight of `10 * 9.81 newton`.

