On considère l'affectation suivante : 

```python
panier = ['Mangue', 'Letchi', 'Papaye', 'Banane']
```

Écrire un programme Python permettant de calculer l'indice de la première occurrence d'un fruit donné dans `panier`.
