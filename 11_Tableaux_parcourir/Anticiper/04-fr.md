On considère l'affectation suivante :

```python
panier = ['Mangue', 'Letchi', 'Papaye', 'Banane']
```

Écrire un programme Python permettant d'indiquer si un fruit donné est dans `panier`.
