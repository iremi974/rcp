Quels sont les messages affichés à l'exécution du programme Python suivant ?

```python
print("Carrés des premiers entiers")
for i in range(10):
    print (i * i)
```

