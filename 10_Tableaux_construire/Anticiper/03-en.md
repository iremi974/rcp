The following assignment is considered:

``python
fruits = ['Mangue', 'Letchi', 'Papaye', 'Orange', 'Banane']
````

Write an instruction to replace the element "Orange" with "Tangor".

