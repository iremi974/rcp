Quelle est la valeur de la variable `p` à la fin de l'exécution du programme suivant ?
```python
p = [10, 11, 12, 13, 14, 15]
p[1] = 22
p[3] = 26
p[5] = 30
```
