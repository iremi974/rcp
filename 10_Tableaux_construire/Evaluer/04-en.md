What is the value of the `p` variable at the end of the next program execution?
``python
p = [10], 11, 12, 13, 14, 15]
p[1] = 22
p[3] = 26
p[5] = 30
````

