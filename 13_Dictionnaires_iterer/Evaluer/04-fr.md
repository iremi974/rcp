Quels sont les messages affichés à l’exécution du programme Python suivant ?

```python
fruits = {'pommes': 4, 'melons': 3, 'poires': 6, 'clémentines': 16}
for v in fruits.values():
    print(v)
```

