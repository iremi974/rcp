Écrire un programme Python utilisant un dictionnaire, permettant de calculer le nombre d'occurrences de chaque nucléotide `A`, `C`, `G`, `T` dans le brin d'ADN suivant :

```python
adn = "ACGGTAGTCAGTACGTGCTAGTACATG"
```
