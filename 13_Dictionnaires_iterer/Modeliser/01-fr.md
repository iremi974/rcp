On considère le dictionnaire suivant : 

```python
repertoire = {'John': '0692123456', 'Anita': '0693102030', 'Aïcha': '0692987654', 'Jean': '0102030405'}
```

Choisir et nommer une ou des variable.s pour parcourir le dictionnaire `repertoire` avec une boucle `for ... in ...`
afin d'afficher les prénoms des contacts enregistrés.
