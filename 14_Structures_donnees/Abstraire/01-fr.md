Réécrire le programme suivant en faisant abstraction de l'implémentation de la structure de données `temps` qui permet d'encapsuler heures, minutes et secondes.
Nommer les fonctions permettant de manipuler un temps pour le construire, l'afficher sous forme de chaine ou effectuer une opération.

```python
somme = 0
h1, mn1, s1 = int(input("h ?")), int(input("mn ?")), int(input("s ?"))
h2, mn2, s2 = int(input("h ?")), int(input("mn ?")), int(input("s ?"))
somme = h1*3600 + mn1*60 + s1 + h2*3600 + mn2*60 + s2
print(str(somme//3600)+'h'+ str((somme//60) % 60) + 'mn' + str(somme % 60) + 's')
```

Le programme affiche la somme de deux temps saisis successivement au clavier.
