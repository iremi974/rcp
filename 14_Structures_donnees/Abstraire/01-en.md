Rewrite the following program, ignoring the implementation of the `time' data structure, which allows you to encapsulate hours, minutes and seconds.
Name the functions to manipulate a time to build it, display it as a string or perform an operation.

``python
sum = 0
h1, mn1, s1 = int(input("h?")), int(input("mn?")), int(input("s?"))
h2, mn2, s2 = int(input("h?")), int(input("mn?")), int(input("s?"))
sum = h1*3600 + mn1*60 + s1 + h2*3600 + mn2*60 + s2
print(str(sum//3600)+'h'+ str(sum//60) % 60) + 'mn' + str(sum % 60) + 's')
````

The program displays the sum of two times entered successively on the keyboard.

