Write a sum recursive function, which, given a `n` positive integer, calculates and returns the sum: `1 + 2 + ... n`

Care will be taken to define the function in a simple case, then in the general case to recall the function for a smaller value.

For example: `sum(9)` shall return `45` and `sum(10)` shall return `55`.


