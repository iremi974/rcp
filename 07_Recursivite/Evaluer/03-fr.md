Tracer les appels à la fonction suivante à l'évaluation de `fib(4)`

```python
def fib(n):
    return 1 if n == 0 or n == 1 else fib(n - 1) + fib(n - 2)
```

