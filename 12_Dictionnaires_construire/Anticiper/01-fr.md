Écrire une expression permettant d'accéder à la quantité de pommes dans le dictionnaire `fruits` suivant :
```python
fruits = {'poires': 5, 'pommes': 11, 'bananes': 7, 'abricots' : 12}
```

