Rewrite the following program by encapsulating the information `p1_x, p1_y, p2_x, p2_y, mid_x, mid_y` inside tuples:

``python
p1_x = 12
p1_y = 8
p2_x = 30
p2_y = 14
middle_x = (p1_x + p2_x) / 2
medium_y = (p1_y + p2_y) / 2
print("The middle of the 2 points has for coordinates", middle_x, middle_y)
````

