Rewrite the expression `9.81 * 30**2 / 2` which calculates the distance in metres travelled by an object in freefall after 30 seconds, replacing constant 30 with a more general notation.


