Encapsuler tous les traitements manipulant des `Temps` dans une classe à ajouter au programme suivant. Modifier le programme principal pour utiliser cette classe.

```python
def temps (h, mn, s):
    return (h*60 + mn)*60 + s
def ajoute (t1, t2):
    return t1 + t2
def affiche (t):
    return str(t//3600)+'h'+ str((t//60) % 60) + 'mn' + str(t%60) + 's'

somme = temps(0,0,0)
h1, mn1, s1 = int(input("h ?")), int(input("mn ?")), int(input("s ?"))
t1 = temps(h1, mn1, s1)
h2, mn2, s2 = int(input("h ?")), int(input("mn ?")), int(input("s ?"))
t2 = temps(h2, mn2, s2)
somme = ajoute(t1, t2)
print(affiche(somme))
```

Le programme affiche la somme de deux temps saisis successivement au clavier.
