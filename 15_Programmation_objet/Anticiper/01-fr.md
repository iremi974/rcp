Implémenter les méthodes de la classe `Raquette` d'un jeu de pong spécifiée ainsi :

```python
class Raquette:
    def init (raquette, position):
    """Positionne la raquette à une position donnée"""

    def droite (raquette):
    """Déplace la raquette vers la droite"""
    
    def gauche (raquette):
    """Déplace la raquette vers la gauche"""
    
    def affiche (raquette)
    """Affiche la raquette à l'écran"""
```
On peut, pour simplifier, réaliser l'affichage de la raquette sur une ligne de la console.
