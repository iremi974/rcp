Implémenter les méthodes de la classe `Pile` spécifiée ainsi :

```python
class Pile:
    def init (pile):
    """Initialise une pile vide"""

    def empiler(pile, element):
    """Empile un élément sur la pile"""
    
    def depiler (pile):
    """Dépile l'élément au sommet et le renvoie"""
    
    def est_vide (pile)
    """Renvoie True si et seulement si la pile est vide"""
```
