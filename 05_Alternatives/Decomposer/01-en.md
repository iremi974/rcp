Decompose the following problem by distinguishing cases requiring special treatment. The number of solutions of a second degree equation of type: ax<sup>2</sup> + bx + c = 0, is different depending on whether the discriminant value (b<sup>2</sup> - 4ac) is strictly positive, zero or strictly negative. Write the general structure of a python program for solving a second degree equation.

