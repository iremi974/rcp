Quel est le message affiché à l'exécution du programme Python suivant ?

```python
m = "Bonjour"
if m[1] == m[5] :
    print("Gagné")
else :
    print("Perdu")
```
