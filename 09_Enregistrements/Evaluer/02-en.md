The following records are considered:

```python
france = {'country': {'name': 'france', 'code': 'FRA'},
'currency': {'name': 'euro', 'code': 'EUR'},
'geo': {'area': 672051, 'population': 64531444}}
```

What are the values of the following expressions?

```python
France
france['name']
france['geo']
france['geo']['population']
```
